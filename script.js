var containsDuplicate = function(nums) {
    if(nums.length <= 1){
        return false;
    }

    let set = new Set(nums);
    let setLength = set.size;
    let arrLength = nums.length;
    return setLength !== arrLength;
};

const answer1 = containsDuplicate([1,2,3,1])
const answer2 = containsDuplicate([1,2,3,4])
const answer3 = containsDuplicate([1,1,1,3,3,4,3,2,4,2])

console.log({
    answer1,
    answer2,
    answer3
})